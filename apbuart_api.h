#ifndef _APBUART_API_H_
#define _APBUART_API_H_

#include <stdint.h>

typedef struct {
  volatile uint32_t data;
  volatile uint32_t status;
  volatile uint32_t control;
  volatile uint32_t scaler;
  volatile uint32_t debug;
  volatile uint32_t time_msb;
} apbuart_regs;

#define APBUART_STATUS_RCNT 0xfc000000
#define APBUART_STATUS_TCNT 0x03f00000
#define APBUART_STATUS_RF   (1<<10)
#define APBUART_STATUS_TF   (1<< 9)
#define APBUART_STATUS_RH   (1<< 8)
#define APBUART_STATUS_TH   (1<< 7)
#define APBUART_STATUS_FE   (1<< 6)
#define APBUART_STATUS_PE   (1<< 5)
#define APBUART_STATUS_OV   (1<< 4)
#define APBUART_STATUS_BR   (1<< 3)
#define APBUART_STATUS_TE   (1<< 2)
#define APBUART_STATUS_TS   (1<< 1)
#define APBUART_STATUS_DR   (1<< 0)

#define APBUART_CTRL_FA (1<<32)
#define APBUART_CTRL_FM (1<<17)
#define APBUART_CTRL_ER (1<<16)
#define APBUART_CTRL_ET (1<<15)
#define APBUART_CTRL_SI (1<<14)
#define APBUART_CTRL_DI (1<<13)
#define APBUART_CTRL_BI (1<<12)
#define APBUART_CTRL_DB (1<<11)
#define APBUART_CTRL_RF (1<<10)
#define APBUART_CTRL_TF (1<< 9)
#define APBUART_CTRL_EC (1<< 8)
#define APBUART_CTRL_LB (1<< 7)
#define APBUART_CTRL_FL (1<< 6)
#define APBUART_CTRL_PE (1<< 5)
#define APBUART_CTRL_PS (1<< 4)
#define APBUART_CTRL_TI (1<< 3)
#define APBUART_CTRL_RI (1<< 2)
#define APBUART_CTRL_TE (1<< 1)
#define APBUART_CTRL_RE (1<< 0)

#endif /* _APBUART_API_H_ */
