# uIP 0.9 driver for Gaisler GRETH #

## Subject ##
The IP core GRETH (and GRETH_GBIT) which are available from [Gobham Gaisler](http://gaisler.com) have drivers for almost every possible OS, including RTEMS, eCos, Linux... Alas I've run into situation when the hardware is limited with on-FPGA memory only, so it is not enough memory to run a mature OS on such device.

A solution could be uIP TCP/IP stack, which is bundled with BCC cross compiler (also available from Gobham Gaisler). Unfortunately, it does not seem to be supported, and the supplied example failed to work on GR-UT699 development board.

The dirty and ugly solution is presented in this repository.

## Build and run requirements ##
First of all, you need a sparc-elf-gcc compiler (also known as BCC). I use the 4.xx branch. You also need standard linux build tools like bash, make.

To run the resulting application you need some board that has Leon3 processor with IRQ controller, GRTIMER and (of course) GRETH (or GRETH_GBIT) IP cores installed.

## How to build and run ##
This is as simple as just run
```
#!sh
make
```

from the top directory. The resulting file, `main.elf`, is a web server that listens to address `192.168.0.2:80`. Load the binary to development board using `grmon`, run it and open the link in web browser. 