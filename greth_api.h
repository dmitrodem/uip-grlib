#ifndef _GRETH_API_H_
#define _GRETH_API_H_

#include <stdint.h>
/* Ethernet configuration registers */
typedef struct {
  volatile uint32_t	control;        
  volatile uint32_t	status;        
  volatile uint32_t	esa_msb;        
  volatile uint32_t	esa_lsb;        
  volatile uint32_t	mdio;        
  volatile uint32_t	tx_desc_p;        
  volatile uint32_t	rx_desc_p;        
  volatile uint32_t	edclip;
} greth_regs;

typedef struct {
  volatile uint32_t control;
  volatile uint32_t buf;
} greth_dma;

#define GRETH_CTRL_EA 0x80000000
#define GRETH_CTRL_BS 0x70000000
#define GRETH_CTRL_GA 0x08000000
#define GRETH_CTRL_MA 0x04000000
#define GRETH_CTRL_MC 0x02000000
#define GRETH_CTRL_ED 0x00004000
#define GRETH_CTRL_RD 0x00002000
#define GRETH_CTRL_DD 0x00001000
#define GRETH_CTRL_ME 0x00000800
#define GRETH_CTRL_PI 0x00000400
#define GRETH_CTRL_BM 0x00000200
#define GRETH_CTRL_GB 0x00000100
#define GRETH_CTRL_SP 0x00000080
#define GRETH_CTRL_RS 0x00000040
#define GRETH_CTRL_PM 0x00000020
#define GRETH_CTRL_FD 0x00000010
#define GRETH_CTRL_RI 0x00000008
#define GRETH_CTRL_TI 0x00000004
#define GRETH_CTRL_RE 0x00000002
#define GRETH_CTRL_TE 0x00000001

#define GRETH_STATUS_PS 0x00000100
#define GRETH_STATUS_IA 0x00000080
#define GRETH_STATUS_TS 0x00000040
#define GRETH_STATUS_TA 0x00000020
#define GRETH_STATUS_RA 0x00000010
#define GRETH_STATUS_TI 0x00000008
#define GRETH_STATUS_RI 0x00000004
#define GRETH_STATUS_TE 0x00000002
#define GRETH_STATUS_RE 0x00000001

#define GRETH_RXDMA_MC		0x04000000
#define GRETH_RXDMA_IF		0x02000000
#define GRETH_RXDMA_TR		0x01000000
#define GRETH_RXDMA_TD		0x00800000
#define GRETH_RXDMA_UR		0x00400000
#define GRETH_RXDMA_UD		0x00200000
#define GRETH_RXDMA_IR		0x00100000
#define GRETH_RXDMA_ID		0x00080000
#define GRETH_RXDMA_LE		0x00040000
#define GRETH_RXDMA_OE		0x00020000
#define GRETH_RXDMA_CE		0x00010000
#define GRETH_RXDMA_FT		0x00008000
#define GRETH_RXDMA_AL		0x00004000
#define GRETH_RXDMA_IE		0x00002000
#define GRETH_RXDMA_WR		0x00001000
#define GRETH_RXDMA_EN		0x00000800
#define GRETH_RXDMA_LENGTH	0x000007ff

#define GRETH_TXDMA_UC		(1<<20)
#define GRETH_TXDMA_TC		(1<<19)
#define GRETH_TXDMA_IC		(1<<18)
#define GRETH_TXDMA_MO		(1<<17)
#define GRETH_TXDMA_LC		(1<<16)
#define GRETH_TXDMA_AL		(1<<15)
#define GRETH_TXDMA_UE		(1<<14)
#define GRETH_TXDMA_IE		(1<<13)
#define GRETH_TXDMA_WR		(1<<12)
#define GRETH_TXDMA_EN		(1<<11)
#define GRETH_TXDMA_LENGTH	0x000007ff

#endif /* _GRETH_API_H_ */
