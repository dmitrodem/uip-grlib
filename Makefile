CC      := sparc-elf-gcc
SIZE    := sparc-elf-size
CFLAGS  := -Wall -O2 -g3 -Iuip
LDFLAGS := -T./linkscript -lsmall

SRCS := uip/uip_arp.c uip/uip.c uip_arch.c \
	uip/httpd/fs.c uip/httpd/cgi.c uip/httpd/httpd.c \
	main.c
OBJS := $(patsubst %.c,%.o,$(SRCS))
DEPS := $(patsubst %.c,%.d,$(SRCS))
clean := $(OBJS) $(DEPS)

main.elf: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^
	$(SIZE) $@
clean += main.elf

-include $(DEPS)

$(OBJS): %.o: %.c
	$(CC) $(CFLAGS) -MMD -c -o $@ $< 

clean:
	-rm -rf $(clean)

.PHONY: clean
