#include "uip/uip.h"
#include "uip/uip_arch.h"

void void_call(void) {
}

void uip_add32(u8_t *op32, u16_t op16) {
  u32_t r = (op32[0]<<24)|(op32[1]<<16)|(op32[2]<<8)|op32[3];
  r += (u32_t) op16;
  uip_acc32[0] = (r >> 24) & 0xff;
  uip_acc32[1] = (r >> 16) & 0xff;
  uip_acc32[2] = (r >>  8) & 0xff;
  uip_acc32[3] = (r >>  0) & 0xff;
}

u16_t uip_chksum(u16_t *buf, u16_t len) {
  return 0;
}

u16_t uip_ipchksum(void) {
  return (u16_t) (-1);
}

u16_t uip_tcpchksum(void) {
  return (u16_t) (-1);
}
