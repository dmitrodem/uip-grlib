#ifndef _LOADMEM_H_
#define _LOADMEM_H_

#include <stdint.h>

inline uint32_t loadmemw(uint32_t *addr){
    uint32_t tmp;        
    asm volatile(" lda [%1]1, %0 "
        : "=r"(tmp)
        : "r"(addr)
        );
    return tmp;
}

inline uint16_t loadmemh(uint16_t *addr){
    uint16_t tmp;        
    asm volatile(" lduha [%1]1, %0 "
        : "=r"(tmp)
        : "r"(addr)
        );
    return tmp;
}

inline uint8_t loadmemb(uint8_t *addr){
    uint8_t tmp;        
    asm volatile(" lduba [%1]1, %0 "
        : "=r"(tmp)
        : "r"(addr)
        );
    return tmp;
}

inline void savememw(uint32_t *addr, uint32_t value){
    asm volatile(" sta %0, [%1]1 "
		 : 
		 : "r"(value), "r"(addr)
        );
}

inline void savememh(uint16_t *addr, uint16_t value){
    asm volatile(" stha %0, [%1]1 "
		 : 
		 : "r"(value), "r"(addr)
        );
}

inline void savememb(uint8_t *addr, uint8_t value){
    asm volatile(" stba %0, [%1]1 "
		 : 
		 : "r"(value), "r"(addr)
        );
}

inline void ormemw(uint32_t *addr, uint32_t mask){
  uint32_t tmp = loadmemw(addr);
  savememw(addr, tmp | mask);
}

inline void ormemh(uint16_t *addr, uint16_t mask){
  uint16_t tmp = loadmemh(addr);
  savememh(addr, tmp | mask);
}

inline void ormemb(uint8_t *addr, uint8_t mask){
  uint8_t tmp = loadmemb(addr);
  savememb(addr, tmp | mask);
}


#define memb() {asm volatile("" ::: "memory");}

#define DCACHE_INVALIDATE_ALL()  {					\
    __asm__ __volatile__ ("flush");					\
    __asm__ __volatile__ ("sta %%g0, [%%g0] 0x11" ::: "memory");	\
  }

#endif /* _LOADMEM_H_ */
