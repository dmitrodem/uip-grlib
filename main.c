#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <asm-leon/amba.h>
#include <asm-leon/irq.h>
#include <asm-leon/timer.h>

#include <time.h>
#include <string.h>

#include "apbuart_api.h"
#include "greth_api.h"
#include "loadmem.h"

#include "uip/uip.h"
#include "uip/uip_arp.h"

#define BUF ((struct uip_eth_hdr *)&uip_buf[0])

#define REG(x) ((uint32_t *) &(x))

#define GRETH_RX_BUF_SIZE 1522
#define GRETH_TX_BUF_SIZE 1522

/* should be a power of 2 */
#define GRETH_RXBD_NUM 32
#define GRETH_TXBD_NUM 32

/* add extra 1 bit to counters to distinguish between full and empty cases */
#define GRETH_RXBD_MASK (((GRETH_RXBD_NUM - 1) << 1) | 0x1)
#define GRETH_TXBD_MASK (((GRETH_TXBD_NUM - 1) << 1) | 0x1)

/* receive and transmit descriptor tables */
volatile greth_dma rxbd[128] __attribute__((section(".desc")));
volatile greth_dma txbd[128] __attribute__((section(".desc")));

/* receive and transmit buffers */
volatile uint8_t rxb[GRETH_RX_BUF_SIZE * GRETH_RXBD_NUM];
volatile uint8_t txb[GRETH_TX_BUF_SIZE * GRETH_TXBD_NUM];

struct greth_driver {
  greth_regs *reg;
  unsigned long irq;
  volatile uint32_t irq_cnt;
  volatile uint8_t irq_pending;
  volatile greth_dma *rxbd;
  volatile greth_dma *txbd;
  volatile uint8_t *rxb;
  volatile uint8_t *txb;
  volatile uint8_t rx_wp;
  volatile uint8_t rx_rp;
  volatile uint8_t rx_pending;
  volatile uint32_t rx_calls;
  volatile uint8_t tx_wp;
  volatile uint32_t timer;
};


#define INCR_RX_PTR(x, c) (x) = (((x) + (c)) & GRETH_RXBD_MASK)
#define INCR_TX_PTR(x, c) (x) = (((x) + (c)) & GRETH_TXBD_MASK)

#define RX_FULL(g)  ((((g)->rx_wp - (g)->rx_rp) & GRETH_RXBD_MASK) == GRETH_RXBD_NUM)
#define RX_EMPTY(g) ((((g)->rx_wp - (g)->rx_rp) & GRETH_RXBD_MASK) == 0)
#define RX_COUNT(g)  (((g)->rx_wp - (g)->rx_rp) & GRETH_RXBD_MASK)

#define RX_UNMASK(x) ((x) & (GRETH_RXBD_MASK >> 1))

struct greth_driver greth[1];

void greth_init(struct greth_driver *gr);
void greth_irq(int irq);
void greth_receive_handle_payload (struct greth_driver *gr);
void dump_packet();
int  do_tick();
void greth_rxdma_rearm();

void greth_enqueue_send(struct greth_driver *gr);
void greth_flush_send(struct greth_driver *gr);
void greth_poll_send(struct greth_driver *gr);

extern uint32_t *console;

int main() {
  
  leonbare_init_ticks();
  ticker_callback = do_tick;
  greth_init(greth);

  console[3] = 0x6;
  
  printf("uIP-0.9 example web server\n");
  uip_init();
  httpd_init();

  while(1){
    if (greth->irq_pending) {
      greth->irq_pending = 0;
      greth_rxdma_rearm();
    }
  }
  return 0;
}

void greth_init(struct greth_driver *gr){
  gr->reg = (greth_regs *) amba_find_apbslv_addr(VENDOR_GAISLER, GAISLER_ETHMAC, &gr->irq);
  {
    /* reset MAC */
    uint32_t tmp;
    tmp = loadmemw(REG(gr->reg->control));
    tmp |= GRETH_CTRL_RS;
    savememw(REG(gr->reg->control), tmp);
    while (loadmemw(REG(gr->reg->control)) & GRETH_CTRL_RS);

    /* set MAC address */
    savememw(REG(gr->reg->esa_msb),
	     (uip_ethaddr.addr[0] << 8) |
	     (uip_ethaddr.addr[1]));
    savememw(REG(gr->reg->esa_lsb),
	     (uip_ethaddr.addr[2] << 24) |
	     (uip_ethaddr.addr[3] << 16) | 
	     (uip_ethaddr.addr[4] <<  8) |
	     (uip_ethaddr.addr[5] <<  0));

    /* force 1000 FD */
    tmp = loadmemw(REG(gr->reg->control));
    tmp = GRETH_CTRL_GB | GRETH_CTRL_FD;
    savememw(REG(gr->reg->control), tmp);
  }

  /* clear status register */
  savememw(REG(gr->reg->status), (uint32_t)(-1));
  
  gr->rxbd = rxbd; gr->txbd = txbd;
  gr->rxb  = rxb;  gr->txb  = txb;
  gr->rx_wp = 0; gr->rx_wp = 0;
  gr->irq_cnt = 0;
  gr->rx_calls = 0;
  gr->tx_wp = 0;
  gr->timer = 0;
  {
    /* setup rx descriptor table */
    size_t i;
    for (i = 0; i < GRETH_RXBD_NUM; i++){
      gr->rxbd[i].control = (i == (GRETH_RXBD_NUM - 1)) ?
	(GRETH_RXDMA_IE | GRETH_RXDMA_EN | GRETH_RXDMA_WR) :
	(GRETH_RXDMA_IE | GRETH_RXDMA_EN);
      gr->rxbd[i].buf     = (uint32_t)&rxb[i*GRETH_RX_BUF_SIZE];
    }
    gr->reg->rx_desc_p = (uint32_t)gr->rxbd;
  }
  {
    /* setup tx descriptor table */
    size_t i;
    for (i = 0; i < GRETH_TXBD_NUM; i++){
      gr->txbd[i].control = 0;
      gr->txbd[i].buf     = (uint32_t) &txb[i*GRETH_TX_BUF_SIZE];
    }
    gr->reg->tx_desc_p = (uint32_t) gr->txbd;
  }
  gr->irq_pending = 0; gr->rx_pending = 0;
  catch_interrupt((int)greth_irq, gr->irq);
  leonbare_enable_irq(gr->irq);

  {
    /* enable receiver */
    uint32_t tmp = loadmemw(REG(gr->reg->control));
    tmp |= GRETH_CTRL_RE | GRETH_CTRL_RI;
    savememw(REG(gr->reg->control), tmp);
  }
}

void greth_irq(int irq){
  greth->irq_pending = 1;
  greth->irq_cnt++;
}

void greth_receive_handle_payload (struct greth_driver *gr){
  if (BUF->type == UIP_ETHTYPE_IP) {
    uip_arp_ipin();
    uip_input();
    if (uip_len > 0) {
      uip_arp_out();
      greth_enqueue_send(gr);
    }
  } else if (BUF->type == UIP_ETHTYPE_ARP) {
    uip_arp_arpin();
    if (uip_len > 0){
      greth_enqueue_send(gr);
    }
  }
}

/**
 * set up TX descriptor. Warning: check gr->tx_wp after call,
 * and flush if it equals GRETH_TXBD_NUM
 */
void greth_enqueue_send(struct greth_driver *gr){
  /* poll until transmitter is inactive */
  greth_poll_send(gr);
  uint8_t *txb = (uint8_t *) &gr->txb[gr->tx_wp * GRETH_TX_BUF_SIZE];
  size_t i;
  for (i = 0; i < 40 + UIP_LLH_LEN; i++){
    txb[i] = uip_buf[i];
  }
  for (; i < uip_len; i++){
    txb[i] = uip_appdata[i - 40 - UIP_LLH_LEN];
  }
  gr->txbd[gr->tx_wp].buf = (uint32_t) txb;
  gr->txbd[gr->tx_wp].control =
    GRETH_TXDMA_UC |
    GRETH_TXDMA_TC |
    GRETH_TXDMA_IC |
    GRETH_TXDMA_EN |
    (GRETH_TXDMA_LENGTH & uip_len);
  gr->tx_wp++;
}

void greth_flush_send(struct greth_driver *gr){
  /* already empty */
  if (gr->tx_wp == 0)
    return;

  savememw(REG(gr->tx_wp), 0);
  /* set wrap flag in last filled descriptor */
  uint32_t tmp;
  tmp = loadmemw(REG(gr->txbd[gr->tx_wp - 1].control));
  savememw(REG(gr->txbd[gr->tx_wp - 1]), tmp | GRETH_TXDMA_WR);

  /* all descriptors up to gr->tx_wp are valid now, so set up GRETH_GBIT APB */
  savememw(REG(gr->reg->tx_desc_p), (uint32_t) gr->txbd);
  savememw(REG(gr->reg->status), GRETH_STATUS_TA | GRETH_STATUS_TI | GRETH_STATUS_TE);

  tmp = loadmemw(REG(gr->reg->control));
  savememw(REG(gr->reg->control), tmp | GRETH_CTRL_TE);
}

void greth_poll_send(struct greth_driver *gr){
  uint32_t tmp;
  do {
    tmp = loadmemw(REG(gr->reg->status));
  } while (tmp & GRETH_CTRL_TE);
}

void uip_log(char *msg) {
  puts(msg);
}

void dump_packet(){
  static uint32_t seq = 0;
  static time_t oldclock = 0;
  time_t current_clock = clock();
  if (seq == 0){
    oldclock = current_clock;
  }
  size_t i;
  printf("\nseq = %lu, time = %lu\n", seq++, current_clock - oldclock);
  for (i = 0; i < uip_len; i++){
    printf("%02x%c", uip_buf[i], (i+1) & 0xf ? ' ' : '\n');
  }
  putchar('\n');
}

int do_tick(){
  int i;
  uint32_t tmp;

  uint32_t uart_status = loadmemw((uint32_t *) 0x80000204);
  if (uart_status & 0x1){
    char ch = getchar();
    switch(ch) {
    case 'r':
      printf("r: %i\n", greth->rx_rp);
      break;
    case 'w':
      printf("w: %i\n", greth->rx_wp);
      break;
    case 'd':
      for (i = 0; i < GRETH_RXBD_NUM; i++){
	tmp = loadmemw(REG(greth->rxbd[i].control));
	printf("[%i] = %08lx\n", i, tmp);
      }
      break;
    case 'i':
      printf("i: %lu\n", greth->irq_cnt);
      break;
    case 'c':
      printf("c: %lu\n", greth->rx_calls);
      break;
    case 'g':
      printf("greth::ctrl   %08lx\n", greth->reg->control);
      printf("greth::status %08lx\n", greth->reg->status);
      printf("greth::txdesc %lu\n", ((greth->reg->tx_desc_p) & 1023) >> 3);
      printf("greth::rxdesc %lu\n", ((greth->reg->rx_desc_p) & 1023) >> 3);
      break;
    default:
      break;
    }
  }

  greth->timer++;
  if ((greth->timer & 127) == 0){
    for (i = 0; i < UIP_CONNS; i++){
      uip_periodic(i);
      if (uip_len > 0){
	uip_arp_out();
	greth_enqueue_send(greth);
	if (greth->tx_wp == GRETH_TXBD_NUM) {
	  greth_flush_send(greth);
	}
      }
    }
    greth_flush_send(greth);
  }

  if ((greth->timer & 1023) == 0){
    uip_arp_timer();
  }
  return 0;
}

void greth_rxdma_rearm(){
  size_t windex = (loadmemw(REG(greth->reg->rx_desc_p)) & 1023) >> 3;
  size_t windex_u;
  size_t i;
  uint32_t control, gr_control;
  uint32_t count = 0;
  size_t start;
  gr_control = loadmemw(REG(greth->reg->control));
  start = (gr_control & GRETH_CTRL_RE) ? 1 : 0;
  for (i = start; i < GRETH_RXBD_NUM; i++){
    windex_u = (windex + i) % GRETH_RXBD_NUM;
    control = loadmemw(REG(greth->rxbd[windex_u].control));
    if (!(control & GRETH_RXDMA_EN)){
      count++;
      /* TODO: process incoming packet here */
      DCACHE_INVALIDATE_ALL();
      uip_buf = (uint8_t *)loadmemw(REG(greth->rxbd[windex_u].buf));
      uip_len = loadmemw(REG(greth->rxbd[windex_u].control)) & GRETH_RXDMA_LENGTH;

      /* handle payload using uIP */
      greth_receive_handle_payload(greth);
      /* all descriptors are used, proceed to flush */
      if (greth->tx_wp == GRETH_TXBD_NUM) {
	greth_flush_send(greth);
      }
      if (windex_u == GRETH_RXBD_NUM - 1) {
	greth->rxbd[windex_u].control = GRETH_RXDMA_EN | GRETH_RXDMA_IE | GRETH_RXDMA_WR;
      } else {
	greth->rxbd[windex_u].control = GRETH_RXDMA_EN | GRETH_RXDMA_IE;
      }
      gr_control = loadmemw(REG(greth->reg->control));
      savememw(REG(greth->reg->control), gr_control | GRETH_CTRL_RE);
    }
  }
  /* flush tx descriptors */
  greth_flush_send(greth);
  //printf("active count = %u\n", count);
  control = loadmemw(REG(greth->reg->control));
  if (!(control & GRETH_CTRL_RE)){
    printf("Too fast!\n");
  }
  savememw(REG(greth->reg->control), control | GRETH_CTRL_RE);
}
